use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{env, metadata, near_bindgen, AccountId, Gas};
use near_sdk::env::log;
use near_sdk::collections::LookupMap;
use std::mem;

mod utils;

#[global_allocator]
static ALLOC: near_sdk::wee_alloc::WeeAlloc<'_> = near_sdk::wee_alloc::WeeAlloc::INIT;

const ONE_NEAR: u128 = 1_000_000_000_000_000_000_000_000;
pub const STORAGE_COST_PER_BYTE: u128 = 100_000_000_000_000_000_000;

/// The max number of times an Account can set their avatar with out a fee
pub const MAX_FREE_SETS: u16 = 2;

// TODO Make this a percentage eventually
pub const SETTER_FEE: u128 = 1_000_000_000_000_000;

type Image = [u32; 32];

// TODO: If the set avatar method is called and the image is not the default set flag to indicate it will cost
#[derive(BorshDeserialize, BorshSerialize)]
pub struct Avatar {
    image: Image
}

#[derive(BorshDeserialize, BorshSerialize)]
pub struct Account {
    account_id: AccountId,
    avatar: Avatar,
    sets: u16, // if true the user is charged a fee to change the avatar
}

impl Account {
    /// Initializes a new Account with...
    pub fn new(account_id: AccountId) -> Self {
        Self {
            account_id: account_id,
            avatar: Avatar {
                // TODO: Gen this based on kechak hash (like blocksies)
                image: [0xffffffff; 32]
            },
            sets: 1,
        }
    }
}

impl ToString for Account {
    fn to_string(&self) -> String {
        let fields = vec![
            self.account_id.to_string(),
            self.sets.to_string(),
        ];
        fields.join("")
    }
}

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize)]
pub struct Nearvatar {
    pub accounts: LookupMap<AccountId, Account>,
}

impl Default for Nearvatar {
    fn default() -> Self {
        Self {
            accounts: LookupMap::new(b"accounts".to_vec()),
        }
    }
}

// Qs:
// - Payable ??
// - Contract upgrade

#[near_bindgen]
impl Nearvatar {
    #[payable]
    /// Allows the contract caller to set an Avatar for its Account
    pub fn set_avatar(&mut self, image: Image) {
        let mut account: Account = self.get_account(env::signer_account_id());
        logger!("{}", &account.to_string());
        if account.sets > MAX_FREE_SETS {
            assert!(
                env::attached_deposit() >= SETTER_FEE,
                "Not enough funds to cover setter fee"
            );
        } else {
            account.sets += 1
        }
        // TODO: Add staking for when setting avatar
        // TODO: Add small fee for setting avatar (free for first 1 0r 2 times?)
        account.avatar.image = image;
        // TODO: Do I need to reinsert this now like so?
        self.accounts.insert(&account.account_id, &account);
        logger!("set_avatar for account_id {}", account.account_id);
    }

    pub fn get_avatar(&self, account_id: AccountId) -> Image{
        assert!(
            env::is_valid_account_id(account_id.as_bytes()),
            "Given account ID is invalid"
        );
        logger!("get_avatar for account_id {}", account_id);
        self.get_account(account_id).avatar.image
    }

    pub fn delete_avatar(&self) {
        /// Retrieve the Account of the method caller
        let mut account: Account = self.get_account(env::signer_account_id());
    }
}

impl Nearvatar {
    fn get_account(&self, account_id: AccountId) -> Account {
        assert!(env::is_valid_account_id(account_id.as_bytes()), "Account ID is invalid");
        self.accounts.get(&account_id).unwrap_or_else(|| Account::new(account_id))
    }
}

#[cfg(not(target_arch = "wasm32"))]
#[cfg(test)]
mod tests {
    use super::*;
    use near_sdk::{MockedBlockchain, Balance};
    use near_sdk::{testing_env, VMContext};

    fn get_context(input: Vec<u8>, is_view: bool, attached_deposit: Balance) -> VMContext {
        VMContext {
            current_account_id: "alice_near".to_string(),
            signer_account_id: "shrute_near".to_string(),
            signer_account_pk: vec![0, 1, 2],
            predecessor_account_id: "carol_near".to_string(),
            input,
            block_index: 0,
            block_timestamp: 0,
            account_balance: 0,
            account_locked_balance: 0,
            storage_usage: 0,
            attached_deposit,
            prepaid_gas: 10u64.pow(18),
            random_seed: vec![0, 1, 2],
            is_view,
            output_data_receivers: vec![],
            epoch_height: 0,
        }
    }

    /// Test that the method caller can set and get their avatar. Additionally, tests that the can
    /// set their avatar for free
    #[test]
    fn set_get_avatar() {
        let context = get_context(vec![], false, 0);
        testing_env!(context);
        let mut contract = Nearvatar::default();
        assert_eq!(env::storage_usage(), 0);
        contract.set_avatar([0xffffffff; 32]);
        logger!("{}", env::storage_usage());
        assert_eq!([0xffffffff; 32], contract.get_avatar("shrute_near".to_string()));
        //assert_eq!(env::storage_usage(), 0);
    }

    /// Test that the method caller must pay a fee to set avatar after 2 sets. In this case the
    /// contract should panic as the caller doesn't have the funds to cver the fee
    #[test]
    #[should_panic(expected = "Not enough funds to cover setter fee")]
    fn charges_set_fee_insufficient_funds() {
        let context = get_context(vec![], false, 0);
        testing_env!(context);
        let mut contract = Nearvatar::default();
        contract.set_avatar([0xffffffff; 32]);
        contract.set_avatar([0xffffffff; 32]);
        contract.set_avatar([0xffffffff; 32]);
    }

    fn charges_set_fee_sufficient_funds() {
        let context = get_context(vec![], false, SETTER_FEE);
        testing_env!(context);
        let mut contract = Nearvatar::default();
        contract.set_avatar([0xfffffff0; 32]);
        contract.set_avatar([0xfffffff1; 32]);
        contract.set_avatar([0xfffffff2; 32]);
        assert_eq!([0xfffffff2; 32], contract.get_avatar("shrute_near".to_string()));
    }
}